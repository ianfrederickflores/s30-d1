const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

/*Connection to MongoDB Atlas
	Syntax:
	mongoose.connect("<MongoDB Atlas connection string>", {useNewUrlParser: true});
*/
mongoose.connect("mongodb+srv://dbIanFlores:Nitefallbrigade2936@wdc028-course-booking.6gyek.mongodb.net/b138_to-do?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;

// If a connection error occured, output in the console
// console.error.bind(console) allow us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Create the Task Model
const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended: true}));

// Create a POST route to create a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {
		// If a document was found and the document's name matches the information sent via client/postman
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate Task Found");
		}

		else{
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}

				else{
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
});

// Create GET request to retreive all the tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		// If an error occured
		if(err){
			return console.log(err);
		}
		// If no errorrs are found
		else{
			return res.status(200).json({
				data: result
			})
		}
	})
});

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
	User.findOne({name: req.body.username}, (err, result) => {
		if(result != null && result.name == req.body.username){
			return res.send("Duplicate User Found");
		}

		else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				}

				else{
					return res.status(201).send("New User Created")
				}
			})
		}
	})
});

app.listen(port, () => console.log(`Server is running at port ${port}`))